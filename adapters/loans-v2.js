const server = require('../server');
const { toPaymentScheduleV2 } = require('./transforms');

server.start({
    host: process.env.ADAPTER_LOANS_HOST || 'loans',
    port: process.env.ADAPTER_LOANS_PORT || '5000',
    pipes: [
        { method: 'get', url: /^\/\d+$/, transformer: transformLoanDetails },
        { method: 'get', url: /^\/\d+\/info$/, transformer: transformLoanInfo },
        { method: 'get', url: /^\/\d+\/schedule$/, transformer: toPaymentScheduleV2 }
    ]
});

function transformLoanDetails(data) {
    if (data == null) return data;
    return Object.assign({}, data, {
        info: transformLoanInfo(data.info),
        schedule: toPaymentScheduleV2(data.schedule)
    });
}

function transformLoanInfo(data) {
    if (data == null) return data;
    delete data.daysDelinquent;
    return data;
}
