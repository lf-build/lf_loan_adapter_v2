module.exports = { toPaymentScheduleV2, toInstallmentV2 };

function toPaymentScheduleV2(schedule) {
    if (schedule == null) return schedule;
    return schedule.map(toInstallmentV2);
}

function toInstallmentV2(i) {
    if (i == null) return i;
    const paydown = i.actual || i.expected;
    return {
        paymentMethod: i.paymentMethod,
        anniversaryDate: i.anniversaryDate,
        dueDate: i.dueDate,
        paymentDate: i.paymentDate,
        paymentId: i.paymentId,
        openingBalance: i.openingBalance,
        endingBalance: i.endingBalance,
        status: translateInstallmentStatus(i.status),
        type: translateInstallmentType(i.type),
        paymentAmount: paydown.amount,
        principal: paydown.principal,
        interest: paydown.interest
    };
}

function translateInstallmentStatus(value) {
    if (value == null) return value;
    if (value.startsWith('Paid') || value === 'Unpaid') return 'Completed';
    return 'Scheduled';
}

function translateInstallmentType(value) {
    if (value === 'Installment') return 'Scheduled';
    if (value === 'Extra') return 'Additional';
    return value;
}
