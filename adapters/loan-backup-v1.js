const server = require('../server');
const { toPaymentScheduleV2, toInstallmentV2 } = require('./transforms');

server.start({
    host: process.env.ADAPTER_LOANBACKUP_HOST || 'loan-backup',
    port: process.env.ADAPTER_LOANBACKUP_PORT || '5000',
    pipes: [
        { method: 'get', url: /^.*$/, transformer: transform }
    ]
});

function transform(body) {
    if (body == null) return body;
    return Array.isArray(body) ? body.map(transformLoanBackup) : transformLoanBackup(body);
}

function transformLoanBackup(backup) {
    if (backup == null) return backup;
    const paymentSchedule = backup.paymentSchedule == null ? null :
    {
        loanReferenceNumber: backup.paymentSchedule.loanReferenceNumber,
        installments: toPaymentScheduleV2(backup.paymentSchedule.installments),
        firstScheduledInstallment: toInstallmentV2(backup.paymentSchedule.firstScheduledInstallment),
        firstInstallment: toInstallmentV2(backup.paymentSchedule.firstInstallment),
        lastPaidInstallment: toInstallmentV2(backup.paymentSchedule.lastPaidInstallment),
        firstInstallment: backup.paymentSchedule.installments.length > 0 ? toInstallmentV2(backup.paymentSchedule.installments[0]) : null
    };
    return Object.assign({}, backup, { paymentSchedule });
}
