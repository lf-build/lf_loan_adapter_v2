FROM node:7.5

ADD . /app
WORKDIR /app
RUN npm install

ENTRYPOINT node adapters/loans-v2.js
