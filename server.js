const http = require('http');
const request = require('request');
const restify = require('restify');
const bunyan = require('bunyan');
const logger = bunyan.createLogger({ name: 'api' });

module.exports = { start };

const methods = ['post','get','post','put','del','head','opts','patch'];

function start(settings) {
    if (settings == null) throw new Error('Argument to parameter `settings` is either null or undefined');
    if (settings.host == null) throw new Error('Argument to parameter `settings.host` is either null or undefined');
    if (settings.port == null) throw new Error('Argument to parameter `settings.port` is either null or undefined');
    if (isBlank(settings.host)) throw new Error('Argument to parameter `settings.host` is empty');
    if (isBlank(settings.port)) throw new Error('Argument to parameter `settings.port` is empty');
    if (settings.pipes == null) throw new Error('Argument to parameter `settings.pipes` is either null or undefined');
    if (!Array.isArray(settings.pipes)) throw new Error('Argument to parameter `settings.pipes` must be an array');

    restify.CORS.ALLOW_HEADERS.push('Authorization', 'authorization');

    const client = restify.createClient({
        url: `http://${settings.host}:${settings.port}`
    });

    const server = restify.createServer({
        name: 'adapter',
        log: bunyan.createLogger({ name: 'server' })
    });

    server.use(restify.requestLogger());
    server.use(restify.CORS());

    server.on('after', restify.auditLogger({ log: bunyan.createLogger({ name: 'request' }) }));

    settings.pipes.forEach(pipe => addPipe(pipe, server, settings));
    
    methods.forEach(method => 
        server[method].call(server, /^.*$/, directPipe(settings))
    );

    server.listen(5000, () => {
        server.log.info('%s listening at %s - forwarding requests to %s:%s', server.name, server.url, settings.host, settings.port);
    });
}

function directPipe(settings) {
    return function(req, res, next) {
        req.pipe(request[req.method.toLowerCase()](`http://${settings.host}:${settings.port}${req.url}`))
            .on('error', err => {
                logger.error(err, 'Error during direct pipe');
                next(internalServerError());
            })
            .pipe(res);
    }
}

function addPipe(pipe, server, settings) {
    if (pipe == null) throw new Error('Argument to parameter `pipe` is either null or undefined');
    if (server == null) throw new Error('Argument to parameter `server` is either null or undefined');
    if (settings == null) throw new Error('Argument to parameter `settings` is either null or undefined');
    if (isBlank(pipe.method)) throw new Error('Argument to parameter `pipe.method` is either null or blank');
    if (isBlank(pipe.url)) throw new Error('Argument to parameter `pipe.url` is either null or blank');
    if (pipe.transformer == null) throw new Error('Argument to parameter `pipe.transformer` is either null or undefined');
    if (!(pipe.transformer instanceof Function)) throw new Error('Argument to parameter `pipe.transformer` must be a function');

    logger.info('Setting up transformer pipe: %s', pipe.url);

    if (!methods.includes(pipe.method))
        throw new Error('Invalid http method: ' + pipe.method);
    
    server[pipe.method].call(server, pipe.url, createPipe(pipe.transformer, settings));
}

function createPipe(transformer, settings) {
    if (transformer == null) throw new Error('Argument to parameter `transformer` is either null or undefined');
    if (!(transformer instanceof Function)) throw new Error('Argument to parameter `transformer` must be a function');

    return function (req, res, next) {
        req.pipe(request[req.method.toLowerCase()](`http://${settings.host}:${settings.port}${req.url}`))
            .on('error', err => {
                logger.error(err, 'Error while piping through transformer');
                next(internalServerError());
            })
            .on('response', response => {
                let data = '';
                response
                    .on('error', err => {
                        logger.error(err, 'Error while piping through transformer');
                        next(internalServerError());
                    })
                    .on('data', chunk => data += chunk)
                    .on('end', () => {
                        const body = JSON.parse(data);
                        res.send(response.statusCode, isSuccess(response) ? transformer(body) : body);
                        next();
                    });
            });
    };
}

function isSuccess(res) {
    return res.statusCode >= 200 && res.statusCode < 300;
}

function internalServerError() {
    return new restify.errors.InternalServerError('An unexpected error has occurred');
}

function isBlank(s) {
    return s == null || s.toString().trim().length === 0;
}
